﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml;
using System.Text;

public class Test_www : MonoBehaviour {

	// Use this for initialization
	void Start () { 
		StartCoroutine (LoadWWW());
	}
	
	IEnumerator LoadWWW(){
		
		string url = "http://localhost:2345/";
		Text hud_text = transform.FindChild("Text").GetComponent<Text>() ; 
		hud_text.text = "connect to FoodAnalyzer ...\n";
		
		while (true){
			WWW www = new WWW(url); 
			while (!www.isDone)
			{            
				yield return new WaitForEndOfFrame();
			}
			
			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.LogWarning(www.error);
				hud_text.text = www.error;
				
				yield return new WaitForSeconds(1f);
			}
			else {    
				
				XmlDocument receivedXML = new XmlDocument();
				StringBuilder result = new StringBuilder(hud_text.text);
				
				if (!www.text.Contains("FOOD")){
					yield return new WaitForSeconds(1f);
					continue;
				}
				
				receivedXML.LoadXml (www.text);
				
				//		string wwwtext = ASCIIEncoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,Encoding.ASCII,www.bytes));
				
				Debug.Log("text received: "+www.text);
				
				foreach (var node in receivedXML.SelectNodes ("/CODES/FOOD")){
					XmlElement foodNode = (XmlElement)node;
					result.AppendFormat ("{0}-FOOD: {1}\n", foodNode.GetAttribute("type"), foodNode.GetAttribute("code"));
				}
				
				foreach (var node in receivedXML.SelectNodes ("/CODES/ROTTEN/FOOD")){
					XmlElement foodNode = (XmlElement)node;
					result.AppendFormat ("allready eaten {0}-FOOD: {1}\n", foodNode.GetAttribute("type"), foodNode.GetAttribute("code"));
				}
				
				hud_text.text = result.ToString();
			
			}
			
			yield return new WaitForSeconds (0.1f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
