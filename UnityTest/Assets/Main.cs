﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Main : MonoBehaviour {

	public int numberOfQuads = 10;
	private Transform sphere;
	private GameObject directionHelperGO ;
	private LineRenderer forceDirectionLine;
	private float lastPower = 0;
	
	Text debugText ;

	// Use this for initialization
	void Start () {
		sphere = transform.FindChild("Sphere");
		sphere.GetComponent<Rigidbody>().useConeFriction = true;
		forceDirectionLine = sphere.GetComponent<LineRenderer>();
		
		debugText = GameObject.Find("Canvas").transform.FindChild("Text").GetComponent<Text>();
		directionHelperGO = new GameObject();
		
		GameObject quadPrefab = Resources.Load<GameObject>("Quad");
		
		for (int i=0; i<numberOfQuads; i++){
			GameObject quad = Instantiate<GameObject>(quadPrefab);
			float ypos = Mathf.Sin ((float)i/(float)numberOfQuads * 2f * Mathf.PI);
			float zpos = Mathf.Cos ((float)i/(float)numberOfQuads * 2* Mathf.PI);
			
			float phi = Mathf.Atan2 (ypos, zpos ) * 180f / Mathf.PI + 180f;
			
			quad.transform.position = new Vector3(0, ypos, zpos);
			quad.transform.eulerAngles = new Vector3(phi, quad.transform.rotation.eulerAngles.y, quad.transform.rotation.eulerAngles.z);
//			quad.transform.localScale = Vector3.one;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float power = -Input.GetAxis("Horizontal");// * -1000 * Time.deltaTime;
		
		if (Mathf.Abs (power) < 0.1f){
			return;
		}
		else {
			if (Mathf.Sign (lastPower) == Mathf.Sign (power) ){
				power = 0f;
				return;
			}
			lastPower = power;
		}
		
		Vector3 force = power * directionHelperGO.transform.up.normalized * 1000;
		
		float phi = Mathf.Atan2 (sphere.position.y, -sphere.position.z ) * 180f / Mathf.PI + 180f;
		directionHelperGO.transform.eulerAngles = new Vector3(phi,0,0);
		sphere.GetComponent<Rigidbody>().AddForce (force, ForceMode.Impulse);
		debugText.text = sphere.GetComponent<Rigidbody>().velocity.ToString("n3");
		
		forceDirectionLine.SetPosition(0, sphere.position);
		forceDirectionLine.SetPosition(1, sphere.position + directionHelperGO.transform.up);
	}
	
	
}
