﻿using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.OCR;
using Emgu.CV.UI;
using Emgu.CV.Structure;
using System.Drawing;
using System.Timers;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;

namespace FoodAnalyzer
{
	public class CameraEye
	{

		public CameraEye ()
		{
		}

		public void Stop()
		{
			_capture.Stop ();
			ReleaseData ();
		}

		public void Start()
		{
			Init ();
		}


		private void Init(){

			if (codes == null) {
				try {
					codes = new Dictionary<FoodType, HashSet<Food>>();
					string codesPath = "food_codes.xml";
					var codesXML = FoodUtil.ReadFoodCodesFile (codesPath);

					foreach (var foodNode in codesXML.SelectNodes("//"+FoodUtil.XML_TAG_FOOD)) {
						XmlElement foodXML = (XmlElement)foodNode;
						FoodType foodType = (FoodType)Enum.Parse (typeof(FoodType), foodXML.GetAttribute (FoodUtil.XML_ATTRIB_type));
						HashSet<Food> foodSet;
						if (!codes.TryGetValue (foodType, out foodSet)) {
							foodSet = new HashSet<Food> ();
							codes [foodType] = foodSet;
						}

						foodSet.Add (new Food(foodXML));

					}

				} catch (Exception ex) {

					System.Console.WriteLine (ex.ToString ());
				}
			}


			try
			{
//				_capture = new Capture(CaptureType.QT);
//				_capture.ImageGrabbed += ProcessFrame;
//				_capture.Start();
			}
			catch (Exception excpt)
			{
				System.Console.WriteLine(excpt.ToString());
			}

			try
			{
				//tess = new Tesseract("tessdata", "eng", Tesseract.OcrEngineMode.OEM_DEFAULT);
				_ocr = new Tesseract();
				#if OSX 
				_ocr.Init("", "eng", OcrEngineMode.TesseractCubeCombined);
				#else
//					_ocr.Init("D:\\temp\\tessdata", "eng", OcrEngineMode.TesseractCubeCombined);
				#endif



				_ocr.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
				_ocr.SetVariable("language_model_penalty_non_dict_word", "0");
				_ocr.SetVariable("language_model_penalty_non_freq_dict_word", "0");
				_ocr.SetVariable("tessedit_pageseg_mode", "7");

				Mat loaded_test_img = new Mat("food_test.jpg", LoadImageType.AnyColor);

				ProcessImage (loaded_test_img);

				//tess.PageSegMode = PageSegMode.SparseText;
			}
			catch (Exception e)
			{
				System.Console.WriteLine(e.ToString());
			}


		}


		public Dictionary<FoodType,HashSet<Food>> codes;
		public HashSet<Food> stomach = new HashSet<Food> ();
		public HashSet<Food> mouth = new HashSet<Food> ();

		private Capture _capture = null;
		private Tesseract _ocr;

		public static string lastCode = "no code";

		private void ProcessImage (Mat img)
		{

			try
			{

				List<IInputOutputArray> codeImagesList = new List<IInputOutputArray>();
				List<IInputOutputArray> filteredCodeImagesList = new List<IInputOutputArray>();
				List<RotatedRect> codeBoxList = new List<RotatedRect>();
				List<string> words = DetectCode(
					img,
					codeImagesList,
					filteredCodeImagesList,
					codeBoxList);


				if (words.Count > 0)
				{		
					lastCode = "code: "+String.Join(", ",words);
					System.Console.WriteLine("code: "+String.Join(", ",words));
				}
			}
			catch (Exception e)
			{
				System.Console.WriteLine (e.Message);
			}

		}

		private void ProcessFrame(object sender, EventArgs arg)
		{
			Mat frame = new Mat ();
			_capture.Retrieve (frame, 0);

			ProcessImage (frame);
		}
//

		private void ReleaseData()
		{
			if (_capture != null)
				_capture.Dispose();
		}
//
//
//		/// <summary>
//		/// Compute the white pixel mask for the given image. 
//		/// A white pixel is a pixel where:  satuation &lt; 40 AND value &gt; 200
//		/// </summary>
//		/// <param name="image">The color image to find white mask from</param>
//		/// <returns>The white pixel mask</returns>
//		private static Image<Gray, Byte> GetWhitePixelMask(Image<Bgr, byte> image)
//		{
//			using (Image<Hsv, Byte> hsv = image.Convert<Hsv, Byte>())
//			{
//				Image<Gray, Byte>[] channels = hsv.Split();
//
//				try
//				{
//					//channels[1] is the mask for satuation less than 40, this is the mask for either white or black pixels
//					channels[1]._ThresholdBinaryInv(new Gray(40), new Gray(255));
//
//					//channels[2] is the mask for bright pixels
//					channels[2]._ThresholdBinary(new Gray(200), new Gray(255));
//
//					CvInvoke.cvAnd(channels[1], channels[2], channels[0], IntPtr.Zero);
//				}
//				finally
//				{
//					channels[1].Dispose();
//					channels[2].Dispose();
//				}
//				return channels[0];
//			}
//		}

		/// <summary>
		/// Detect license plate from the given image
		/// </summary>
		/// <param name="img">The image to search license plate from</param>
		/// <param name="licensePlateImagesList">A list of images where the detected license plate regions are stored</param>
		/// <param name="filteredLicensePlateImagesList">A list of images where the detected license plate regions (with noise removed) are stored</param>
		/// <param name="detectedLicensePlateRegionList">A list where the regions of license plate (defined by an MCvBox2D) are stored</param>
		/// <returns>The list of words for each license plate</returns>
		public List<String> DetectCode(
			IInputArray img, 
			List<IInputOutputArray> licensePlateImagesList, 
			List<IInputOutputArray> filteredLicensePlateImagesList, 
			List<RotatedRect> detectedLicensePlateRegionList)
		{
			List<String> licenses = new List<String>();
			using (Mat gray = new Mat())
			using (Mat canny = new Mat())
			using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
			{
				CvInvoke.CvtColor(img, gray, ColorConversion.Bgr2Gray);
				CvInvoke.Canny(gray, canny, 200, 20, 3, false);//(gray, canny, 100, 50, 3, false);
				int[,] hierachy = CvInvoke.FindContourTree(canny, contours, ChainApproxMethod.ChainApproxSimple);

				FindLicensePlate(contours, hierachy, 0, gray, canny, licensePlateImagesList, filteredLicensePlateImagesList, detectedLicensePlateRegionList, licenses);
			}
			return licenses;
		}

		private static int GetNumberOfChildren(int[,] hierachy, int idx)
		{
			//first child
			idx = hierachy[idx,2];
			if (idx < 0)
				return 0;

			int count = 1;
			while (hierachy[idx,0] > 0)
			{
				count++;
				idx = hierachy[idx,0];
			}
			return count;
		}

		private static int plateNum = 0;
		private void FindLicensePlate(
			VectorOfVectorOfPoint contours, int[,] hierachy, int idx, IInputArray gray, IInputArray canny,
			List<IInputOutputArray> licensePlateImagesList, List<IInputOutputArray> filteredLicensePlateImagesList, List<RotatedRect> detectedLicensePlateRegionList,
			List<String> licenses)
		{
			for (; idx >= 0;  idx = hierachy[idx,0])
			{
				int numberOfChildren = GetNumberOfChildren(hierachy, idx);      
				//if it does not contains any children (charactor), it is not a license plate region
				if (numberOfChildren == 0) continue;

				using (VectorOfPoint contour = contours[idx])
				{
					if (CvInvoke.ContourArea(contour) > 400)
					{
						if (numberOfChildren < 3)
						{
							//If the contour has less than 3 children, it is not a license plate (assuming license plate has at least 3 charactor)
							//However we should search the children of this contour to see if any of them is a license plate
							FindLicensePlate(contours, hierachy, hierachy[idx, 2], gray, canny, licensePlateImagesList,
								filteredLicensePlateImagesList, detectedLicensePlateRegionList, licenses);
							continue;
						}

						RotatedRect box = CvInvoke.MinAreaRect(contour);
						if (box.Angle < -45.0)
						{
							float tmp = box.Size.Width;
							box.Size.Width = box.Size.Height;
							box.Size.Height = tmp;
							box.Angle += 90.0f;
						}
						else if (box.Angle > 45.0)
						{
							float tmp = box.Size.Width;
							box.Size.Width = box.Size.Height;
							box.Size.Height = tmp;
							box.Angle -= 90.0f;
						}

						double whRatio = (double) box.Size.Width/box.Size.Height;
						if (!(3f < whRatio && whRatio < 7f))
							//if (!(1.0 < whRatio && whRatio < 2.0))
						{
							//if the width height ratio is not in the specific range,it is not a license plate 
							//However we should search the children of this contour to see if any of them is a license plate
							//Contour<Point> child = contours.VNext;
							if (hierachy[idx, 2] > 0)
								FindLicensePlate(contours, hierachy, hierachy[idx, 2], gray, canny, licensePlateImagesList,
									filteredLicensePlateImagesList, detectedLicensePlateRegionList, licenses);
							continue;
						}

						using (UMat tmp1 = new UMat())
						using (UMat tmp2 = new UMat())
						{
							PointF[] srcCorners = box.GetVertices();

							PointF[] destCorners = new PointF[] {
								new PointF(0, box.Size.Height - 1),
								new PointF(0, 0),
								new PointF(box.Size.Width - 1, 0), 
								new PointF(box.Size.Width - 1, box.Size.Height - 1)};

							using (Mat rot = CvInvoke.GetAffineTransform(srcCorners, destCorners))
							{
								CvInvoke.WarpAffine(gray, tmp1, rot, Size.Round(box.Size));           
							}

							//resize the license plate such that the front is ~ 10-12. This size of front results in better accuracy from tesseract
							Size approxSize = new Size(240, 180);
							double scale = Math.Min(approxSize.Width/box.Size.Width, approxSize.Height/box.Size.Height);
							Size newSize = new Size( (int)Math.Round(box.Size.Width*scale),(int) Math.Round(box.Size.Height*scale));
							CvInvoke.Resize(tmp1, tmp2, newSize, 0, 0, Inter.Cubic);

							//removes some pixels from the edge
							int edgePixelSize = 2;
							Rectangle newRoi = new Rectangle(new Point(edgePixelSize, edgePixelSize),
								tmp2.Size - new Size(2*edgePixelSize, 2*edgePixelSize));
							UMat plate = new UMat(tmp2, newRoi);

							UMat filteredPlate = FilterPlate(plate);

							Tesseract.Character[] words;
							StringBuilder strBuilder = new StringBuilder();
							using (UMat tmp = filteredPlate.Clone())
							{
								_ocr.Recognize(tmp);
								words = _ocr.GetCharacters();

								if (words.Length == 0) continue;

								tmp.Save ("code_" + (plateNum++) + ".jpg");

								for (int i = 0; i < words.Length; i++)
								{
									//Encoding.BigEndianUnicode
									//words[i].Text.
									strBuilder.Append(words[i].Text);
								}
							}

							licenses.Add(strBuilder.ToString());
							licensePlateImagesList.Add(plate);
							filteredLicensePlateImagesList.Add(filteredPlate);
							detectedLicensePlateRegionList.Add(box);

						}
					}
				}
			}
		}

		/// <summary>
		/// Filter the license plate to remove noise
		/// </summary>
		/// <param name="plate">The license plate image</param>
		/// <returns>License plate image without the noise</returns>
		private static UMat FilterPlate(UMat plate)
		{
			UMat thresh = new UMat();
			CvInvoke.Threshold(plate, thresh, 120, 255, ThresholdType.BinaryInv);
			//Image<Gray, Byte> thresh = plate.ThresholdBinaryInv(new Gray(120), new Gray(255));

			Size plateSize = plate.Size;
			using (Mat plateMask = new Mat(plateSize.Height, plateSize.Width, DepthType.Cv8U, 1))
			using (Mat plateCanny = new Mat())
			using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
			{
				plateMask.SetTo(new MCvScalar(255.0));
				CvInvoke.Canny(plate, plateCanny, 100, 50);
				CvInvoke.FindContours(plateCanny, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

				int count = contours.Size;
				for (int i = 1; i < count; i++)
				{
					using (VectorOfPoint contour = contours[i])
					{

						Rectangle rect = CvInvoke.BoundingRectangle(contour);
						if (rect.Height > (plateSize.Height >> 1))
						{
							rect.X -= 1; rect.Y -= 1; rect.Width += 2; rect.Height += 2;
							Rectangle roi = new Rectangle(Point.Empty, plate.Size);
							rect.Intersect(roi);
							CvInvoke.Rectangle(plateMask, rect, new MCvScalar(), -1);
							//plateMask.Draw(rect, new Gray(0.0), -1);
						}
					}

				}

				thresh.SetTo(new MCvScalar(), plateMask);
			}

			CvInvoke.Erode(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);
			CvInvoke.Dilate(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);

			return thresh;
		}

	}
}

