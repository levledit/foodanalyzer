﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Web;


namespace FoodAnalyzer
{
	public static class FoodUtil
	{
		public const int numCodesPerType = 5;

		public const string resutLabel_prephrase = "Last food: ";

		public const string XML_TAG_FOOD = "FOOD";
		public const string XML_TAG_CODES = "CODES";
		public const string XML_ATTRIB_code = "code";
		public const string XML_ATTRIB_type = "type";
		public const string XML_ATTRIB_poison = "poison";

		public static List<string> vovels = new List<string>(new string[] { "A","E","I","O","U"});
		public static List<string> consonants = new List<string>(new string[]{ "B","C","D","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z"});

		private static Random rand = new Random();

		public static Dictionary<string,string[]> evolving = new Dictionary<string, string[]>(){
			{"C",new string[]{"O","Q"}},
			{"E",new string[]{"B"}},
			{"F",new string[]{"E"}},
			{"I",new string[]{"B","D","E","F","J","K","L","N","P","R","T"}},
			{"L",new string[]{"B","E"}},
			{"O",new string[]{"Q"}},
			{"P",new string[]{"B"}},
			{"V",new string[]{"M"}}
		};

		private static List<string> EvolvingList (List<string> letters) {
			List<string> result = new List<string> ();
			foreach (string letter in letters) {
				if (evolving.ContainsKey (letter)) {
					result.Add (letter);
				}
			}

			return result;
		}

		public static XmlDocument CreateXML (Food food)
		{
			var doc = new XmlDocument ();
			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);

			XmlElement root = doc.CreateElement(XML_TAG_FOOD);
			doc.AppendChild (root);

			root.SetAttribute (XML_ATTRIB_code, food.code); 
			root.SetAttribute (XML_ATTRIB_type, food.type.ToString());

			if (food.type == FoodType.MAGIC){
				root.SetAttribute (XML_ATTRIB_poison, food.poison);
			}


			return doc;
		}

		public static string RandomCode (FoodType type){
			
			List<string>[] letters = (type == FoodType.MAGIC)? 
							  new List<string>[]{EvolvingList(vovels), EvolvingList(consonants)} 
							: new List<string>[]{vovels,consonants} ;

			string code = "";
			for (int i = 0; i < 8; i++) {
//				string[] letterArray = i % 3 == 1 ? letters [0] : letters [1];
				List<string> letterArray = letters [i % 2];
				string letter = letterArray [rand.Next (letterArray.Count)];

				code += letter.ToUpper();
			}

			return code;
		}

		public static string RandomPoison (string magic){
			
			string poison = "";
			int changed = 0;
			for (int i = 0; i < magic.Length; i++) {
				string letter = magic [i].ToString ();
				string newLetter;
				//wenn changed == i/2 --> middle = 0.5 wenn changed == i --> middle = 1 wenn changed == 0 middle = 0
				float middle = i==0? 0.5f:(float)changed / (float)i;
				middle = middle / 2f + 0.25f;
				if (rand.NextDouble () > middle) {
					changed++;
					newLetter = evolving [letter] [rand.Next (evolving [letter].Length)];
				} else {
					newLetter = letter;
				}

				poison += newLetter;
			}

			return poison;
		}

		public static XmlDocument ReadFoodCodesFile (string codesPath)
		{
			var doc = new XmlDocument ();
			FileInfo codesFile = new FileInfo(codesPath);
			if (!codesFile.Exists) {
				doc = CreateCodes (codesPath, numCodesPerType);
				doc.Save (codesPath);
			} else {
				doc.Load (codesPath);
			}

			return doc;
		}

		public static XmlDocument CreateCodes (string codesPath, int codesPerType = 100)
		{
			var doc = new XmlDocument ();
			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);
			XmlElement root = doc.CreateElement (XML_TAG_CODES);
			doc.AppendChild (root);

			XmlElement timeStampXML = doc.CreateElement ("stamp");
			timeStampXML.SetAttribute ("time", DateTime.Now.ToString());
			root.AppendChild (timeStampXML);

			foreach (FoodType type in Enum.GetValues(typeof (FoodType))){
				if (type == FoodType.POISON) {
					continue;
				}

				for (int i = 0; i < codesPerType; i++) {

					XmlElement foodXml = doc.CreateElement (XML_TAG_FOOD);
					root.AppendChild (foodXml);
					string code = RandomCode (type);
					if (type == FoodType.MAGIC) {
						string poison = RandomPoison (code);
						foodXml.SetAttribute (XML_ATTRIB_poison, poison);
						var poisonXML = doc.CreateElement (XML_TAG_FOOD);
						poisonXML.SetAttribute (XML_ATTRIB_code, poison);
						poisonXML.SetAttribute (XML_ATTRIB_type, FoodType.POISON.ToString());
						root.AppendChild (poisonXML);
					}
					foodXml.SetAttribute (XML_ATTRIB_code, code);
					foodXml.SetAttribute (XML_ATTRIB_type, type.ToString());

				}
			}

			return doc;
		}
	}
}

