﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FoodAnalyzer.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <asp:Button ID="readButton" runat="server" Text="Read!" OnClick="readButtonClicked" />
            <br />
		    <asp:Label id="resultLabel" runat="server" Text="Last Food: " />
            <br />
            <br />
		    <asp:Label id="errorText" runat="server" Text="no error" />
    </div>
    </form>
</body>
</html>
