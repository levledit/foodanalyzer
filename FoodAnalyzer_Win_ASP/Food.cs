﻿using System;
using System.Xml;

namespace FoodAnalyzer
{
	public class Food
	{
		public string code;
		public string poison;
		public FoodType type;


		public Food (XmlElement foodXML)
		{
			code = foodXML.GetAttribute (FoodUtil.XML_ATTRIB_code);
			type = (FoodType)Enum.Parse(typeof (FoodType), foodXML.GetAttribute (FoodUtil.XML_ATTRIB_type));
			if (type == FoodType.MAGIC && foodXML.HasAttribute (FoodUtil.XML_ATTRIB_poison)) {

				poison = foodXML.GetAttribute (FoodUtil.XML_ATTRIB_poison);
			}
		}
	}

	public enum FoodType {
		BEER,
		BANANA,
		POISON,
		MAGIC
	}
}

