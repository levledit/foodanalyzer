﻿using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;

using Emgu.CV;
using Emgu.CV.OCR;
using Emgu.CV.Structure;
using System.Drawing;
using System.Timers;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;

namespace FoodAnalyzer
{
	public class CameraEye
	{

		public const bool DO_Save_Images = false;

		public TimeSpan chewingDuration = new TimeSpan (0, 0, 2);

//		public Dictionary<string, Food> codes;
		public Dictionary<string, HashSet<Food>> codeParts = new Dictionary<string, HashSet<Food>>();
		public HashSet<Food> stomach = new HashSet<Food> (); //food that is eaten and sent to client
		public HashSet<Food> mouth = new HashSet<Food> (); //food that is eaten but not sent to client

		private Dictionary<Food, DateTime> lastBites = new Dictionary<Food, DateTime>();
//		public HashSet<Food> newBitesOldFood = new HashSet<Food>();

		private Dictionary<string, DateTime> chewingParts = new Dictionary<string, DateTime> ();

		//private Capture _capture = null;
		private Tesseract _ocr;

        private bool isProcessingFrame = false;

		public CameraEye ()
		{
			
		}

		public void Stop()
		{
			//_capture.Stop ();
			ReleaseData ();
		}

		public void Start()
		{
			Init ();
		}

        //public void StartCapture()
        //{
        //    _capture.Start();
        //}

		private void Init(){

			ReadCodesFile ();
			ReadSavedData ();

			try
			{
				//tess = new Tesseract("tessdata", "eng", Tesseract.OcrEngineMode.OEM_DEFAULT);
				_ocr = new Tesseract();
				_ocr.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
				_ocr.PageSegMode = PageSegMode.SparseText;

#if OSX
					_ocr.Init("", "eng", OcrEngineMode.Default);
#else
                _ocr.Init("", "eng", OcrEngineMode.TesseractOnly);
#endif



                //				_ocr.SetVariable("language_model_penalty_non_dict_word", "0");
                //				_ocr.SetVariable("language_model_penalty_non_freq_dict_word", "0");
                //				_ocr.SetVariable("tessedit_pageseg_mode", "7");


                //test:
//				using (Mat loaded_test_img = new Mat("food_test.jpg", LoadImageType.AnyColor)) {
//					MainWindow.AddLine("start test 0°");
//					ProcessImage(loaded_test_img);
//				}
//				//test:
//				using (Mat loaded_test_img = new Mat("food_test_90.jpg", LoadImageType.AnyColor)) {
//					MainWindow.AddLine("start test 90°");
//					ProcessImage(loaded_test_img);
//				}
//				//test:
//				using (Mat loaded_test_img = new Mat("food_test_180.jpg", LoadImageType.AnyColor)) {
//					MainWindow.AddLine("start test 180°");
//					ProcessImage(loaded_test_img);
//				}
//				//test:
//				using (Mat loaded_test_img = new Mat("food_test_270.jpg", LoadImageType.AnyColor)) {
//					MainWindow.AddLine("start test 270°");
//					ProcessImage(loaded_test_img);
//				}

                //tess.PageSegMode = PageSegMode.SparseText;
            }
            catch (Exception e)
			{
				System.Console.WriteLine(e.ToString());
			}



			try
			{
				//_capture = new Capture();
				//_capture.ImageGrabbed += ProcessFrame;
				//_capture.Start();
			}
			catch (Exception excpt)
			{
				System.Console.WriteLine(excpt.ToString());
			}
		}

		public void ClearData(){
			mouth.Clear ();
			stomach.Clear ();
			lastBites.Clear ();
			chewingParts.Clear ();
		}

		public void ReadSavedData(){
			string dataPath = "data.xml";
			var doc = new XmlDocument ();
			FileInfo dataFile = new FileInfo(dataPath);
			if (!dataFile.Exists) {
				return;
			}

			doc.Load (dataPath);
			ClearData ();

			foreach (var node in doc.SelectNodes("/SAVEDATA/STOMACH/"+FoodUtil.XML_TAG_FOOD)) {
				stomach.Add (new Food ((XmlElement)node));
			}
			foreach (var node in doc.SelectNodes("/SAVEDATA/MOUTH/"+FoodUtil.XML_TAG_FOOD)) {
				var food = new Food ((XmlElement)node);
				mouth.Add (food);
				lastBites [food] = DateTime.Now;
			}
		}

		public void SaveData(){
			string dataPath = "data.xml";

			var doc = new XmlDocument ();
			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);
			XmlElement root = doc.CreateElement ("SAVEDATA");
			doc.AppendChild (root);

			XmlElement elm = doc.CreateElement ("STOMACH");
			root.AppendChild (elm);
			foreach (var food in stomach) {
				food.AppendToXML (elm);
			}
			elm = doc.CreateElement ("MOUTH");
			root.AppendChild (elm);
			foreach (var food in mouth) {
				food.AppendToXML (elm);
			}

			doc.Save (dataPath);
		}

		public void ReadCodesFile(){
			try {
//				codes = new Dictionary<string, Food>();
				var codesXML = FoodUtil.ReadOrCreateCodesFile ();

				var timeStamp = codesXML.SelectSingleNode("//stamp/@time");
				MainWindow.AddLine("using food_codes: "+timeStamp.Value);

				foreach (var foodNode in codesXML.SelectNodes("//"+FoodUtil.XML_TAG_FOOD)) {
					XmlElement foodXML = (XmlElement)foodNode;
					Food food = new Food(foodXML);

//					foreach (string code in FoodUtil.CreateMinusOneLetterCodes (food.code)){
//						codes[code] = food;
//					}

					foreach (string code in FoodUtil.CreateTrippleCodes (food.code)){
						HashSet<Food> foodList;
						if (!codeParts.TryGetValue (code, out foodList)){
							foodList = new HashSet<Food>();
							codeParts[code] = foodList;
						}
						if (!foodList.Contains(food)) {
							foodList.Add(food);
						}
					}


				}

			} catch (IOException ex) {
				System.Console.WriteLine (ex.ToString ());
			}
		}

		public void ProcessImage (Mat img)
		{
            if (isProcessingFrame)
            {
                return;
            }
            plateNum++;

            isProcessingFrame = true;
            // remove old chewing codes
            DateTime now = DateTime.Now;
			List<string> codesToDelete = new List<string>();
			foreach (var codeTime in chewingParts) {
				if ((now - codeTime.Value).Duration() > chewingDuration) {
					codesToDelete.Add (codeTime.Key);
				}
			}
			foreach (string codeToDelete in codesToDelete) {
				chewingParts.Remove (codeToDelete);
			}

			try
			{

                //img.Save("testimages/code_" + (plateNum) + "_img.jpg");

//                List<IInputOutputArray> codeImagesList = new List<IInputOutputArray>();
//				List<IInputOutputArray> filteredCodeImagesList = new List<IInputOutputArray>();
//				List<RotatedRect> codeBoxList = new List<RotatedRect>();
//				List<string> words = DetectCode(
//					img,
//					codeImagesList,
//					filteredCodeImagesList,
//					codeBoxList);

				if (DO_Save_Images){
					img.Save("testimages/code_" + (plateNum) + "new_image.jpg");
				}

				List<string> words = DetectCode (img);

				for (int i=0; i<3; i++){
					//rotate 90°
					plateNum++;
					using (Mat clone = img.Clone()){
						CvInvoke.Transpose(clone, img);
					}
					if (DO_Save_Images){
						img.Save("testimages/code_" + (plateNum) + "_rot.jpg");
					}
					CvInvoke.Flip(img, img, FlipType.Horizontal);
					if (DO_Save_Images){
						img.Save("testimages/code_" + (plateNum) + "_flip.jpg");
					}

					words.AddRange(DetectCode (img));

				}


				if (words != null && words.Count > 0)
				{
//                    System.Console.WriteLine("found: " + string.Join(",", words));
                    foreach (string code in words) {
						
//						if (code.Length == FoodUtil.numLettersPerCode-1) {
//							EatFood (code);
//						}
//						else if (code.Length == FoodUtil.numLettersPerCode){
//							foreach (string tolerantCode in FoodUtil.CreateMinusOneLetterCodes (code)){
//								if (EatFood (tolerantCode)){
//									break;
//								}
//							}
//						}
//						else if (code.Length >= 3){
						if (code.Length >= 3){
							HashSet<Food> possibleFoodSet = new HashSet<Food>();

							foreach (string codeTripple in FoodUtil.CreateTrippleCodes(code)){
								HashSet<Food> newPossibleFood;
								if (codeParts.TryGetValue(codeTripple, out newPossibleFood)){
									possibleFoodSet.UnionWith (newPossibleFood);
									if (!chewingParts.ContainsKey(codeTripple)){
										chewingParts[codeTripple] = DateTime.Now;
									}
								}
							}

							foreach (Food possibleFood in possibleFoodSet){
							
								UInt16 result = FoodUtil.GetPuzzlePattern(chewingParts.Keys, possibleFood.code);
								int correctLetters = FoodUtil.BitCount(result);
//								System.Console.WriteLine ("correct letters: "+correctLetters+"  part: "+codeTripple+"  code: "+possibleFood.code);

								if (correctLetters >= FoodUtil.numLettersPerCode-1){
									EatFood (possibleFood);
								}
							}
						}

					}

//                    System.Console.WriteLine("code: " + String.Join(", ", words));
                }
            }
			catch (Exception e)
			{
				System.Console.WriteLine (e.ToString());
			}

            isProcessingFrame = false;
        }

		//private void ProcessFrame(object sender, EventArgs arg)
		//{
		//	using (Mat frame = new Mat ()) {
		//		_capture.Retrieve (frame, 0);

		//		if (!frame.IsEmpty) {

		//			ProcessImage (frame);
		//			MainWindow.UpdateFrame (frame);

		//		}
		//	}
		//}



		private bool EatFood (Food food){
			bool eaten = false;
			DateTime lastBite;

			if (!mouth.Contains (food)) {
				if (!stomach.Contains (food)) {
					mouth.Add (food);
					string result = string.Format ("eating {0}-food: {1}", food.type, food.code);
					MainWindow.AddLine (result);
					eaten = true;
				} //else {
//					DateTime lastBite;
//					if (lastBites.TryGetValue (food, out lastBite)) {
//						if ((DateTime.Now - lastBite).Duration () > chewingDuration) {
//
//							string result = string.Format ("allready ate this {0}-food: {1}", food.type, food.code);
//							MainWindow.AddLine (result);
//							mouth.Add (food);
//						}
//					}
//				}
			}

			if (!eaten && lastBites.TryGetValue (food, out lastBite)) {
				if ((DateTime.Now - lastBite).Duration () > chewingDuration) {

					string result = string.Format ("allready ate this {0}-food: {1}", food.type, food.code);
					MainWindow.AddLine (result);
					if (!mouth.Contains (food)) {
						mouth.Add (food);
						eaten = true;
					}
				}
			}

			if (eaten) {
				SaveData ();
			}

			lastBites [food] = DateTime.Now;

			return eaten;
		}

		private void ReleaseData()
		{
			//if (_capture != null)
			//	_capture.Dispose();
		}
//
//
//		/// <summary>
//		/// Compute the white pixel mask for the given image. 
//		/// A white pixel is a pixel where:  satuation &lt; 40 AND value &gt; 200
//		/// </summary>
//		/// <param name="image">The color image to find white mask from</param>
//		/// <returns>The white pixel mask</returns>
//		private static Image<Gray, Byte> GetWhitePixelMask(Image<Bgr, byte> image)
//		{
//			using (Image<Hsv, Byte> hsv = image.Convert<Hsv, Byte>())
//			{
//				Image<Gray, Byte>[] channels = hsv.Split();
//
//				try
//				{
//					//channels[1] is the mask for satuation less than 40, this is the mask for either white or black pixels
//					channels[1]._ThresholdBinaryInv(new Gray(40), new Gray(255));
//
//					//channels[2] is the mask for bright pixels
//					channels[2]._ThresholdBinary(new Gray(200), new Gray(255));
//
//					CvInvoke.cvAnd(channels[1], channels[2], channels[0], IntPtr.Zero);
//				}
//				finally
//				{
//					channels[1].Dispose();
//					channels[2].Dispose();
//				}
//				return channels[0];
//			}
//		}

		/// <summary>
		/// Detect license plate from the given image
		/// </summary>
		/// <param name="img">The image to search license plate from</param>
		/// <param name="licensePlateImagesList">A list of images where the detected license plate regions are stored</param>
		/// <param name="filteredLicensePlateImagesList">A list of images where the detected license plate regions (with noise removed) are stored</param>
		/// <param name="detectedLicensePlateRegionList">A list where the regions of license plate (defined by an MCvBox2D) are stored</param>
		/// <returns>The list of words for each license plate</returns>
//		public List<String> DetectCode(
//			IInputArray img, 
//			List<IInputOutputArray> licensePlateImagesList, 
//			List<IInputOutputArray> filteredLicensePlateImagesList, 
//			List<RotatedRect> detectedLicensePlateRegionList)
		public List<String> DetectCode(
			IInputArray img)
		
		{
			List<String> licenses = new List<String>();
			using (Mat gray = new Mat())
			using (Mat canny = new Mat())
			using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
			{
				CvInvoke.CvtColor(img, gray, ColorConversion.Bgr2Gray);
                CvInvoke.Canny(gray, canny, 100, 50, 3, false);//(gray, canny, 100, 50, 3, false);
                //CvInvoke.Canny(gray, canny, 90, 40, 3, true);//(gray, canny, 100, 50, 3, false);

                if (DO_Save_Images)
                {
                    gray.Save("testimages/code_" + (plateNum) + "_gray.jpg");
                    canny.Save("testimages/code_" + (plateNum) + "_canny.jpg");
                }
                int[,] hierachy = CvInvoke.FindContourTree(canny, contours, ChainApproxMethod.ChainApproxSimple);

//				FindLicensePlate(contours, hierachy, 0, gray, canny, licensePlateImagesList, filteredLicensePlateImagesList, detectedLicensePlateRegionList, licenses);
				FindLicensePlate(contours, hierachy, 0, gray, licenses);
			}
			return licenses;
		}

		private static int GetNumberOfChildren(int[,] hierachy, int idx)
		{
			if (hierachy.GetLength (0) <= idx || hierachy.GetLength (1) <= 2) {
//				Console.WriteLine ("jetzt gaxen!");
				return 0; //TODO: no idea, if this is correct, ...?
			}

			//first child
			idx = hierachy[idx,2];
			if (idx < 0)
				return 0;

			int count = 1;
			while (hierachy[idx,0] > 0)
			{
				count++;
				idx = hierachy[idx,0];
			}
			return count;
		}

		private static int plateNum = 0;
//		private void FindLicensePlate(
//			VectorOfVectorOfPoint contours, int[,] hierachy, int idx, IInputArray gray, IInputArray canny,
//			List<IInputOutputArray> licensePlateImagesList, List<IInputOutputArray> filteredLicensePlateImagesList, List<RotatedRect> detectedLicensePlateRegionList,
//			List<String> licenses)
		private void FindLicensePlate(
			VectorOfVectorOfPoint contours, int[,] hierachy, int idx, IInputArray gray, List<String> licenses)
		
		{
			if (hierachy.GetLength (0) == 0) {
				return;
			}

			for (; idx >= 0;  idx = hierachy[idx,0])
			{
				int numberOfChildren = GetNumberOfChildren(hierachy, idx);      
				//if it does not contains any children (charactor), it is not a license plate region
				if (numberOfChildren == 0) continue;

				using (VectorOfPoint contour = contours[idx])
				{
					if (CvInvoke.ContourArea(contour) > 400)
					{
						if (numberOfChildren < 3)
						{
							//If the contour has less than 3 children, it is not a license plate (assuming license plate has at least 3 charactor)
							//However we should search the children of this contour to see if any of them is a license plate
//							FindLicensePlate(contours, hierachy, hierachy[idx, 2], gray, canny, licensePlateImagesList,
//								filteredLicensePlateImagesList, detectedLicensePlateRegionList, licenses);
							FindLicensePlate(contours, hierachy, hierachy[idx, 2], gray, licenses);
							continue;
						}

						RotatedRect box = CvInvoke.MinAreaRect(contour);
						if (box.Angle < -45.0)
						{
							float tmp = box.Size.Width;
							box.Size.Width = box.Size.Height;
							box.Size.Height = tmp;
							box.Angle += 90.0f;
						}
						else if (box.Angle > 45.0)
						{
							float tmp = box.Size.Width;
							box.Size.Width = box.Size.Height;
							box.Size.Height = tmp;
							box.Angle -= 90.0f;
						}

						double whRatio = (double) box.Size.Width/box.Size.Height;
						if (!(2f < whRatio && whRatio < 7f))
							//if (!(1.0 < whRatio && whRatio < 2.0))
						{
							//if the width height ratio is not in the specific range,it is not a license plate 
							//However we should search the children of this contour to see if any of them is a license plate
							//Contour<Point> child = contours.VNext;




							if (hierachy[idx, 2] > 0)
//								FindLicensePlate(contours, hierachy, hierachy[idx, 2], gray, canny, licensePlateImagesList,
//									filteredLicensePlateImagesList, detectedLicensePlateRegionList, licenses);

									FindLicensePlate(contours, hierachy, hierachy[idx, 2], gray, licenses);
							
							continue;
						}

                        //Console.WriteLine("image " + plateNum + ": " + (int)box.Center.X + "," + (int)box.Center.Y + "," + (int)box.Size.Width + "," + (int)box.Size.Height);

						using (UMat tmp1 = new UMat())
						using (UMat tmp2 = new UMat())
						{
							PointF[] srcCorners = box.GetVertices();

							PointF[] destCorners = new PointF[] {
								new PointF(0, box.Size.Height - 1),
								new PointF(0, 0),
								new PointF(box.Size.Width - 1, 0), 
								new PointF(box.Size.Width - 1, box.Size.Height - 1)};

							using (Mat rot = CvInvoke.GetAffineTransform(srcCorners, destCorners))
							{
								CvInvoke.WarpAffine(gray, tmp1, rot, Size.Round(box.Size));           
							}

                            //resize the license plate such that the front is ~ 10-12. This size of front results in better accuracy from tesseract
                            Size approxSize = new Size(400, 150);
                            //SizeF approxSize = box.Size;
                            double scale = Math.Min(approxSize.Width/box.Size.Width, approxSize.Height/box.Size.Height);
							Size newSize = new Size( (int)Math.Round(box.Size.Width*scale),(int) Math.Round(box.Size.Height*scale));
							CvInvoke.Resize(tmp1, tmp2, newSize, 0, 0, Inter.Cubic);
                            if (DO_Save_Images)
                            {
                                tmp1.Save("testimages/code_" + (plateNum) + "_tmp1.jpg");
                                tmp2.Save("testimages/code_" + (plateNum) + "_tmp2.jpg");
							}

                            //removes some pixels from the edge
							int edgePixelSize = 2;
							Rectangle newRoi = new Rectangle(new Point(edgePixelSize, edgePixelSize),
								tmp2.Size - new Size(2*edgePixelSize, 2*edgePixelSize));
							using (UMat plate = new UMat (tmp2, newRoi))
							using (UMat filteredPlate = FilterPlate (plate)) {

								StringBuilder strBuilder = new StringBuilder ();
								Tesseract.Character[] words;
//								using (UMat tmp = filteredPlate.Clone ())
								/*using (Image tmpRot = tmp.ToImage<tmp.tmp.Depth> ()) */{
//								tmpRot.RotateFlip (RotateFlipType.Rotate180FlipNone);
//
//								tmp.
//								_ocr.Recognize (tmpRot);
									_ocr.Recognize (filteredPlate);

									words = _ocr.GetCharacters ();

									if (DO_Save_Images) {
										plate.Save ("testimages/code_" + (plateNum) + "_p.jpg");
										filteredPlate.Save ("testimages/code_" + (plateNum) + ".jpg");
									}

									if (words.Length == 0)
										continue;


									for (int i = 0; i < words.Length; i++) {
										//Encoding.BigEndianUnicode
										//words[i].Text.
										strBuilder.Append (words [i].Text);
									}
								}

								licenses.Add(strBuilder.ToString());
//								licensePlateImagesList.Add(plate);
//								filteredLicensePlateImagesList.Add(filteredPlate);
//								detectedLicensePlateRegionList.Add(box);

							}

						}
					}
				}
			}
		}

		/// <summary>
		/// Filter the license plate to remove noise
		/// </summary>
		/// <param name="plate">The license plate image</param>
		/// <returns>License plate image without the noise</returns>
		private static UMat FilterPlate(UMat plate)
		{
			UMat thresh = new UMat();
			CvInvoke.Threshold(plate, thresh, 120, 255, ThresholdType.BinaryInv); //120, 255, ThresholdType.BinaryInv);

//			plate.Save ("pt_" + (plateNum++) + "_plate.jpg");
//			thresh.Save ("pt_" + plateNum + "_thresh.jpg");
			//Image<Gray, Byte> thresh = plate.ThresholdBinaryInv(new Gray(120), new Gray(255));

			Size plateSize = plate.Size;
			using (Mat plateMask = new Mat(plateSize.Height, plateSize.Width, DepthType.Cv8U, 1))
			using (Mat plateCanny = new Mat())
			using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
			{
				plateMask.SetTo(new MCvScalar(255.0));
				CvInvoke.Canny(plate, plateCanny, 100, 50);
				CvInvoke.FindContours(plateCanny, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

				int count = contours.Size;
				for (int i = 1; i < count; i++)
				{
					using (VectorOfPoint contour = contours[i])
					{

						Rectangle rect = CvInvoke.BoundingRectangle(contour);
						if (rect.Height > (plateSize.Height >> 1))
						{
							rect.X -= 1; rect.Y -= 1; rect.Width += 2; rect.Height += 2;
							Rectangle roi = new Rectangle(Point.Empty, plate.Size);
							rect.Intersect(roi);
							CvInvoke.Rectangle(plateMask, rect, new MCvScalar(), -1);
							//plateMask.Draw(rect, new Gray(0.0), -1);
						}
					}

				}

				thresh.SetTo(new MCvScalar(), plateMask);
			}

			CvInvoke.Erode(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);
			CvInvoke.Dilate(thresh, thresh, null, new Point(-1, -1), 1, BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);

			return thresh;
		}

	}
}

