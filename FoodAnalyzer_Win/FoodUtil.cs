﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Web;
using System.Text;


namespace FoodAnalyzer
{
	public static class FoodUtil
	{
		public const int numCodesPerType = 25;
		public const int numLettersPerCode = 8;

		public const string codesPath = "food_codes.xml";

		public const string resutLabel_prephrase = "Last food: ";

		public const string XML_TAG_FOOD = "FOOD";
		public const string XML_TAG_CODES = "CODES";
		public const string XML_TAG_ROTTEN = "ROTTEN";
		public const string XML_ATTRIB_code = "code";
		public const string XML_ATTRIB_type = "type";
		public const string XML_ATTRIB_poison = "poison";

		public static List<string> vovels = new List<string>(new string[] { "A","E","I","O","U"});
		public static List<string> consonants = new List<string>(new string[]{ "B","C","D","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z"});

		private static Random rand = new Random();

		public static Dictionary<string,string[]> evolving = new Dictionary<string, string[]>(){
			{"C",new string[]{"O","Q"}},
			{"E",new string[]{"B"}},
			{"F",new string[]{"E"}},
			{"I",new string[]{"B","D","E","F","J","K","L","N","P","R","T"}},
			{"L",new string[]{"B","E"}},
			{"O",new string[]{"Q"}},
			{"P",new string[]{"B"}},
			{"V",new string[]{"M"}}
		};

		private static List<string> EvolvingList (List<string> letters) {
			List<string> result = new List<string> ();
			foreach (string letter in letters) {
				if (evolving.ContainsKey (letter)) {
					result.Add (letter);
				}
			}

			return result;
		}

		public static string[] CreateTrippleCodes (string longCode){
			string[] result = new string[longCode.Length-2];

			for (int i = 0; i < longCode.Length - 2; i++) {
				result [i] = longCode.Substring (i, 3);
			}

			return result;
		}

		public static string[] CreateMinusOneLetterCodes (string longCode){
			string[] result = new string[longCode.Length];

			StringBuilder code = new StringBuilder(longCode.Length-1);
			for (int i=0; i<longCode.Length; i++){
				for (int n=0; n<longCode.Length; n++){
					if (n==i) {
						continue;
					}
					code.Append(longCode[n]);
				}
				result[i] = code.ToString();
				code.Clear();
			}

			return result;
		}

		/// <summary>
		/// Gets the puzzle pattern.
		/// </summary>
		/// <returns>1 at each bit, where letter known in parts.</returns>
		/// <param name="parts">Parts.</param>
		/// <param name="code">Code.</param>
		public static UInt16 GetPuzzlePattern (IEnumerable<string> parts, string code){
			UInt16 pattern = 0;
			foreach (string part in parts) {
				int index = -1;
				do {
					index = code.IndexOf (part, index+1);
					if (index >= 0) {
						pattern |= GetPattern(index, part.Length);
					}
				} while (index >= 0);
			}

			return pattern;
		}

		private static UInt16 GetPattern (int startPos, int numOnes){
			UInt16 pattern = 0;

			for (int i = startPos; i < startPos + numOnes; i++) {
				pattern |= (UInt16)Math.Pow (2f, (float)i);
			}

			return pattern;
		}

		public static string RandomCode (FoodType type){
			
			List<string>[] letters = (type == FoodType.MAGIC)? 
							  new List<string>[]{EvolvingList(vovels), EvolvingList(consonants)} 
							: new List<string>[]{vovels,consonants} ;

			string code = "";
			for (int i = 0; i < numLettersPerCode; i++) {
//				string[] letterArray = i % 3 == 1 ? letters [0] : letters [1];
				List<string> letterArray = letters [i % 2];
				string letter = letterArray [rand.Next (letterArray.Count)];

				code += letter.ToUpper();
			}

			return code;
		}



		const UInt64 m1  = 0x5555555555555555; //binary: 0101...
		const UInt64 m2  = 0x3333333333333333; //binary: 00110011..
		const UInt64 m4  = 0x0f0f0f0f0f0f0f0f; //binary:  4 zeros,  4 ones ...
		const UInt64 m8  = 0x00ff00ff00ff00ff; //binary:  8 zeros,  8 ones ...
		const UInt64 m16 = 0x0000ffff0000ffff; //binary: 16 zeros, 16 ones ...
		const UInt64 m32 = 0x00000000ffffffff; //binary: 32 zeros, 32 ones
		const UInt64 hff = 0xffffffffffffffff; //binary: all ones
		const UInt64 h01 = 0x0101010101010101; //the sum of 256 to the power of 0,1,2,3...

		public static int BitCount(UInt64 x)
		{
			x -= (x >> 1) & m1;             //put count of each 2 bits into those 2 bits
			x = (x & m2) + ((x >> 2) & m2); //put count of each 4 bits into those 4 bits 
			x = (x + (x >> 4)) & m4;        //put count of each 8 bits into those 8 bits 
			x += x >>  8;  //put count of each 16 bits into their lowest 8 bits
			x += x >> 16;  //put count of each 32 bits into their lowest 8 bits
			x += x >> 32;  //put count of each 64 bits into their lowest 8 bits
			return (int)(x & 0x7f);
		}

		public static string RandomPoison (string magic){
			
			string poison = "";
			int changed = 0;
			for (int i = 0; i < magic.Length; i++) {
				string letter = magic [i].ToString ();
				string newLetter;
				//wenn changed == i/2 --> middle = 0.5 wenn changed == i --> middle = 1 wenn changed == 0 middle = 0
				float middle = i==0? 0.5f:(float)changed / (float)i;
				middle = middle / 2f + 0.25f;
				if (rand.NextDouble () > middle) {
					changed++;
					newLetter = evolving [letter] [rand.Next (evolving [letter].Length)];
				} else {
					newLetter = letter;
				}

				poison += newLetter;
			}

			return poison;
		}

		public static XmlDocument ReadOrCreateCodesFile ()
		{
			var doc = new XmlDocument ();
			FileInfo codesFile = new FileInfo(codesPath);
			if (!codesFile.Exists) {
				doc = CreateCodes (codesPath, numCodesPerType);
				doc.Save (codesPath);
			} else {
				doc.Load (codesPath);
			}

			return doc;
		}

		public static void AddCodes (XmlDocument doc, FoodType type, int numCodes){
			if (type == FoodType.POISON) {
				return;
			}

			XmlElement root = doc.DocumentElement;

			for (int i = 0; i < numCodes; i++) {

				XmlElement foodXml = doc.CreateElement (XML_TAG_FOOD);
				root.AppendChild (foodXml);
				string code = RandomCode (type);
				if (type == FoodType.MAGIC) {
					string poison = RandomPoison (code);
					foodXml.SetAttribute (XML_ATTRIB_poison, poison);
					var poisonXML = doc.CreateElement (XML_TAG_FOOD);
					poisonXML.SetAttribute (XML_ATTRIB_code, poison);
					poisonXML.SetAttribute (XML_ATTRIB_type, FoodType.POISON.ToString());
					root.AppendChild (poisonXML);
				}
				foodXml.SetAttribute (XML_ATTRIB_code, code);
				foodXml.SetAttribute (XML_ATTRIB_type, type.ToString());

			}

			((XmlAttribute)root.SelectSingleNode ("stamp/@time")).Value = DateTime.Now.ToString ();
		}

		public static XmlDocument CreateCodes (string codesPath, int codesPerType = 100)
		{
			var doc = new XmlDocument ();
			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);
			XmlElement root = doc.CreateElement (XML_TAG_CODES);
			doc.AppendChild (root);

			XmlElement timeStampXML = doc.CreateElement ("stamp");
			timeStampXML.SetAttribute ("time", DateTime.Now.ToString());
			root.AppendChild (timeStampXML);

			foreach (FoodType type in Enum.GetValues(typeof (FoodType))){
				AddCodes (doc, type, codesPerType);
			}

			return doc;
		}
	}
}

