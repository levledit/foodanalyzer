﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using RedCorona.Net;
using System.Text;
using System.Xml;


namespace FoodAnalyzer
{
	public class FoodServer
	{
		Server server;
		CameraEye cam;
		Random rand;

		public void Start(){
			server = new Server(2345, new ClientEvent(ClientConnect));
			MainWindow.AddLine ("Food Server started.");
		}

		public void Stop(){
			server.Close ();
			MainWindow.AddLine ("Food Server stoped.");
		}

		bool ClientConnect(Server serv, ClientInfo new_client){
			new_client.Delimiter = "\n";
			new_client.OnRead += new ConnectionRead(ReadData);
			return true; // allow this connection
		}

		void ReadData(ClientInfo ci, String text){
//			Console.WriteLine("Received from "+ci.ID+": "+text);

			if (string.IsNullOrWhiteSpace (text)) {

				var doc = new XmlDocument ();
				XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
				doc.AppendChild (docNode);

				XmlElement root = doc.CreateElement (FoodUtil.XML_TAG_CODES);
				doc.AppendChild (root);

				XmlElement rottenXML = null;
				bool sentNewData = false;

				foreach (Food food in cam.mouth) {
					if (cam.stomach.Contains (food)) {
						if (rottenXML == null) {
							rottenXML = doc.CreateElement (FoodUtil.XML_TAG_ROTTEN);
							root.AppendChild (rottenXML);
						}

						food.AppendToXML (rottenXML);
					} else {
						food.AppendToXML (root);
						cam.stomach.Add (food);
						sentNewData = true;
					}
				}
				cam.mouth.Clear ();

				if (sentNewData) {
					cam.SaveData ();
				}

				ci.Send (doc.OuterXml);
				ci.Close ();
			}
		}

		public FoodServer (CameraEye cam)
		{
			this.cam = cam;
			rand = new Random ();
		}

	}
}

