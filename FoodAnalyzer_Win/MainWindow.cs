﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;

namespace FoodAnalyzer
{
    public partial class MainWindow : Form
    {
		private static MainWindow _instance;

        private Capture _capture = null;
        CameraEye cam;
        FoodServer server;

		private StringBuilder textForOutput = new StringBuilder();

        public MainWindow()
        {
			_instance = this;

            InitializeComponent();

            CvInvoke.UseOpenCL = false;
            try
            {
                _capture = new Capture();
                _capture.ImageGrabbed += ProcessFrame;
				_capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, 640);
				_capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, 480);

                cam = new CameraEye();
                //cam.Start();

                server = new FoodServer(cam);

				cam.Start();

				server.Start();

				_capture.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
			using (Mat frame = new Mat ()) {
				_capture.Retrieve (frame, 0);
				_instance.imageCapture.Image = frame;

				cam.ProcessImage (frame);
			}

			_instance.textField.Text += textForOutput.ToString ();
			textForOutput.Clear ();

			GC.Collect ();
            //histogramBox1.AddHistogram("gax", Color.AliceBlue, img, 1, new float[] { 0, 10 });
            //histogramBox1.Refresh();
        }


		private void Reset_Click(object sender, EventArgs e){
			cam.ClearData();
			cam.SaveData ();
			AddLine ("\nAll saved data deleted. (Server reset)\n");
		}

		private void AddCodes_Click(object sender, EventArgs e){
			FoodType type = FoodType.POISON;
			switch (((Button)sender).Name) {
			case "addBananaCodesButton":
				type = FoodType.BANANA;
				break;

			case "addBeerCodesButton":
				type = FoodType.BEER;
				break;

			case "addMagicCodesButton":
				type = FoodType.MAGIC;
				break;
			}
			if (type != FoodType.POISON) {
				var doc = FoodUtil.ReadOrCreateCodesFile ();
				FoodUtil.AddCodes (doc, type, FoodUtil.numCodesPerType);
				doc.Save (FoodUtil.codesPath);
				cam.ReadCodesFile ();
			}
		}

		private void close_Click(object sender, EventArgs e){
			Application.Exit();
		}

        private void Start_Click(object sender, EventArgs e)
        {
            if (cam != null)
            {

                cam.Start();
                //cam.StartCapture();

                server.Start();
                //if (_captureInProgress)
                //{  //stop the capture
                //    captureButton.Text = "Start Capture";
                //    _capture.Pause();
                //}
                //else
                //{
                //    //start the capture
                //    captureButton.Text = "Stop";
                //    _capture.Start();
                //}

                //_captureInProgress = !_captureInProgress;

                _capture.Start();
            }
        }

        public static void AddLine (string text){
			Console.WriteLine (text);
			_instance.textForOutput.AppendLine (text);
			//_instance.textField.Text += text + "\n";
		}
    }
}
