﻿using System;
using System.Xml;

namespace FoodAnalyzer
{
	public class Food: IEquatable<Food>
	{
		public string code;
		public string poison;
		public FoodType type;

		public override int GetHashCode()
		{
			return code.GetHashCode(); // Or something like that
		}

		public override bool Equals(object obj)
		{
			return obj is Food && Equals((Food) obj);
		}

		public bool Equals(Food p)
		{
			return code == p.code;
		}


		public Food (XmlElement foodXML)
		{
			code = foodXML.GetAttribute (FoodUtil.XML_ATTRIB_code);
			type = (FoodType)Enum.Parse(typeof (FoodType), foodXML.GetAttribute (FoodUtil.XML_ATTRIB_type));
			if (type == FoodType.MAGIC && foodXML.HasAttribute (FoodUtil.XML_ATTRIB_poison)) {

				poison = foodXML.GetAttribute (FoodUtil.XML_ATTRIB_poison);
			}
		}



		public XmlElement AppendToXML (XmlElement parent)
		{

			XmlElement foodNode = parent.OwnerDocument.CreateElement(FoodUtil.XML_TAG_FOOD);
			parent.AppendChild (foodNode);

			foodNode.SetAttribute (FoodUtil.XML_ATTRIB_code, code); 
			foodNode.SetAttribute (FoodUtil.XML_ATTRIB_type, type.ToString());

			if (type == FoodType.MAGIC){
				foodNode.SetAttribute (FoodUtil.XML_ATTRIB_poison, poison);
			}


			return foodNode;
		}
	}

	public enum FoodType {
		BEER,
		BANANA,
		POISON,
		MAGIC
	}
}

