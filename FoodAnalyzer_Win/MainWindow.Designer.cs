﻿namespace FoodAnalyzer
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
//            this.button1 = new System.Windows.Forms.Button();
			this.closeButton = new System.Windows.Forms.Button();
			this.resetButton = new System.Windows.Forms.Button();
			this.addBananaCodesButton = new System.Windows.Forms.Button();
			this.addBeerCodesButton = new System.Windows.Forms.Button();
			this.addMagicCodesButton = new System.Windows.Forms.Button();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.textField = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
//            this.imageSmall = new Emgu.CV.UI.ImageBox();
            this.imageCapture = new Emgu.CV.UI.ImageBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.SuspendLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.imageSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCapture)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(784, 562);
            this.splitContainer1.SplitterDistance = 201;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
//            this.splitContainer4.Panel1.Controls.Add(this.button1);
			this.splitContainer4.Panel1.Controls.Add(this.closeButton);
			this.splitContainer4.Panel1.Controls.Add(this.resetButton);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.vScrollBar1);
            this.splitContainer4.Panel2.Controls.Add(this.textField);
            this.splitContainer4.Size = new System.Drawing.Size(784, 201);
            this.splitContainer4.SplitterDistance = 261;
            this.splitContainer4.TabIndex = 0;
            // 
            // button1
            // 
//            this.button1.Location = new System.Drawing.Point(88, 12);
//            this.button1.Name = "button1";
//            this.button1.Size = new System.Drawing.Size(75, 23);
//            this.button1.TabIndex = 0;
//            this.button1.Text = "Start";
//            this.button1.UseVisualStyleBackColor = true;
//			this.button1.Click += new System.EventHandler(this.Start_Click);
			// 
			// close Button
			// 
			this.closeButton.Location = new System.Drawing.Point(88, 45);
			this.closeButton.Name = "closeButton";
			this.closeButton.Size = new System.Drawing.Size(75, 23);
			this.closeButton.TabIndex = 0;
			this.closeButton.Text = "Close";
			this.closeButton.UseVisualStyleBackColor = true;
			this.closeButton.Click += new System.EventHandler(this.close_Click);
			// 
			// reset Button
			// 
			this.resetButton.Location = new System.Drawing.Point(40, 78);
			this.resetButton.Name = "resetButton";
			this.resetButton.Size = new System.Drawing.Size(175, 23);
			this.resetButton.TabIndex = 0;
			this.resetButton.Text = "Reset (Delete Saved Data)";
			this.resetButton.UseVisualStyleBackColor = true;
			this.resetButton.Click += new System.EventHandler(this.Reset_Click);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar1.Location = new System.Drawing.Point(502, 0);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(17, 201);
            this.vScrollBar1.TabIndex = 1;
            // 
            // textField
            // 
            this.textField.AutoSize = true;
            this.textField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textField.Location = new System.Drawing.Point(0, 0);
            this.textField.Name = "textField";
            this.textField.Size = new System.Drawing.Size(0, 13);
            this.textField.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.imageCapture);
            this.splitContainer2.Size = new System.Drawing.Size(784, 357);
            this.splitContainer2.SplitterDistance = 261;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 

			this.splitContainer3.Panel1.Controls.Add(this.addBananaCodesButton);
			this.splitContainer3.Panel1.Controls.Add(this.addBeerCodesButton);
			this.splitContainer3.Panel1.Controls.Add(this.addMagicCodesButton);

            this.splitContainer3.Size = new System.Drawing.Size(261, 357);
            this.splitContainer3.SplitterDistance = 350;
            this.splitContainer3.TabIndex = 0;


			// 
			// banana Button
			// 
			this.addBananaCodesButton.Location = new System.Drawing.Point(40, 10);
			this.addBananaCodesButton.Name = "addBananaCodesButton";
			this.addBananaCodesButton.Size = new System.Drawing.Size(175, 23);
			this.addBananaCodesButton.TabIndex = 0;
			this.addBananaCodesButton.Text = "Add "+ FoodUtil.numCodesPerType +" Banana Codes";
			this.addBananaCodesButton.UseVisualStyleBackColor = true;
			this.addBananaCodesButton.Click += new System.EventHandler(this.AddCodes_Click);
			// 
			// beer Button
			// 
			this.addBeerCodesButton.Location = new System.Drawing.Point(40, 43);
			this.addBeerCodesButton.Name = "addBeerCodesButton";
			this.addBeerCodesButton.Size = new System.Drawing.Size(175, 23);
			this.addBeerCodesButton.TabIndex = 1;
			this.addBeerCodesButton.Text = "Add "+ FoodUtil.numCodesPerType +" Beer Codes";
			this.addBeerCodesButton.UseVisualStyleBackColor = true;
			this.addBeerCodesButton.Click += new System.EventHandler(this.AddCodes_Click);
			// 
			// magic Button
			// 
			this.addMagicCodesButton.Location = new System.Drawing.Point(40, 76);
			this.addMagicCodesButton.Name = "addMagicCodesButton";
			this.addMagicCodesButton.Size = new System.Drawing.Size(175, 23);
			this.addMagicCodesButton.TabIndex = 2;
			this.addMagicCodesButton.Text = "Add "+ FoodUtil.numCodesPerType +" Magic Codes";
			this.addMagicCodesButton.UseVisualStyleBackColor = true;
			this.addMagicCodesButton.Click += new System.EventHandler(this.AddCodes_Click);
//            // 
//            // imageSmall
//            // 
//            this.imageSmall.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.imageSmall.Location = new System.Drawing.Point(0, 0);
//            this.imageSmall.Name = "imageSmall";
//            this.imageSmall.Size = new System.Drawing.Size(261, 86);
//            this.imageSmall.TabIndex = 2;
//            this.imageSmall.TabStop = false;
            // 
            // imageCapture
            // 
            this.imageCapture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageCapture.Location = new System.Drawing.Point(0, 0);
            this.imageCapture.Name = "imageCapture";
            this.imageCapture.Size = new System.Drawing.Size(519, 357);
            this.imageCapture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imageCapture.TabIndex = 2;
            this.imageCapture.TabStop = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainWindow";
            this.Text = "Food Analyzer";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
//            ((System.ComponentModel.ISupportInitialize)(this.imageSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCapture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private Emgu.CV.UI.ImageBox imageCapture;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.Label textField;
//        private Emgu.CV.UI.ImageBox imageSmall;
//        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button addBananaCodesButton;
        private System.Windows.Forms.Button addBeerCodesButton;
        private System.Windows.Forms.Button addMagicCodesButton;
    }
}

