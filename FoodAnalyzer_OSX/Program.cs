﻿using System;
using System.Windows.Forms;

namespace FoodAnalyzer
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			MainWindow win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}
	}
}
